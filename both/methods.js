"use strict";

Store.methods = {};

/**
 * Insert a user
 * the data is validated against the SimpleSchema
 */
Store.methods.insert = new ValidatedMethod({
    name: "Store.methods.insert",
    validate: Store.schema.validator(),
    run(user) {
        console.log("insert user into collection", user);

        if (Meteor.isServer) {
            Store.insert(user);
        }
    }
});

/**
 * Server side check of the email availability
 * this is done to avoid having to publish the whole list
 * of emails to the clients
 */
Store.methods.emailIsAvailable = new ValidatedMethod({
    name: "Store.methods.emailIsAvailable",
    validate: null,
    run(email) {
        console.log("Store.methods.emailIsAvailable", email);

        if (Meteor.server) {
            return !Store.find({email: email}).count();
        }
    }
});
