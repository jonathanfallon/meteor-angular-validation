"use strict";

angular
    .module("Validation")
    .controller("ViewCtrl", ViewCtrl)
    .directive("noNumbers", noNumbers)
    .directive("customEmail", customEmail)
    .directive("checkExistence", checkExistence);

function ViewCtrl($scope, $reactive) {
    $reactive(this).attach($scope);

    /**
     * Subscribe to users publication
     */
    $scope.users = null;
    $scope.$meteorSubscribe("store");
    Tracker.autorun(function () {
        if ($scope.users === null) {
            $scope.users = Store.find().fetch();
        }
        if (!$scope.$$phase) {
            $scope.$apply(function () {
                $scope.users = Store.find().fetch();
            });
        }
    });

    /**
     * onSubmit function called on form submission
     */
    $scope.onSubmit = (user, form) => {
        /**
         * kill if form isn't valid
         */
        if (form.$invalid) {
            throw "Invalid form submitted";
        }

        /**
         * kill if the user object is undefined
         * shouldn't happen with the $invalid before
         * but we're never sure...
         */
        if (angular.isUndefined(user)) {
            throw "Undefined user object";
        }

        /**
         * Call the insert method from Meteor with the user data
         * It will be validated against the Schema
         */
        Meteor.call("Store.methods.insert", user);
    };

}

/**
 * Directive custom de vérification de nombres
 * retourne false si on trouve des nombres
 */
function noNumbers() {
    return {
        require: "ngModel",
        link (scope, element, attrs, ctrl) {
            ctrl.$validators["no-numbers"] = (modelValue, viewValue) => {
                if (ctrl.$isEmpty(modelValue)) {
                    return false;
                }

                /**
                 * Validation function defined in Meteor collection file
                 */
                return Store.validators.noNumbers(modelValue);
            }
        }
    };
}

/**
 * Custom email validation directive
 * overwrite the default Angular type="email" check
 * e.g. https://docs.angularjs.org/guide/forms
 */
function customEmail() {
    return {
        require: "?ngModel",
        link(scope, element, attrs, ctrl) {
            if (ctrl && ctrl.$validators.email) {
                ctrl.$validators.email = (modelValue) => {
                    return ctrl.$isEmpty(modelValue) || Store.validators.email(modelValue);
                }
            }
        }
    }
}

/**
 * Check email existence on a fake remote service
 */
function checkExistence($q, $timeout) {
    return {
        require: "ngModel",
        link(scope, element, attrs, ctrl) {
            if (ctrl) {
                ctrl.$asyncValidators.existence = (modelValue, viewValue) => {
                    console.log('checkExistence', modelValue);
                    if (ctrl.$isEmpty(modelValue)) {
                        return $q.when();
                    }

                    let def = $q.defer();

                    Meteor.call("Store.methods.emailIsAvailable", modelValue, (err, result) => {
                        if (!result) {
                            def.reject();
                        }

                        def.resolve();
                    });

                    return def.promise;
                }
            }
        }
    }
}