"use strict";

angular
    .module("Validation")
    .config(($stateProvider, $urlRouterProvider) => {
        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state("root", {
                url: "/",
                templateUrl: "client/view.html",
                controller: "ViewCtrl"
            });
    });
