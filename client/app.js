"use strict";

angular
    .module('Validation', ['angular-meteor', 'ngMessages', 'ionic']);

angular.element(document).ready(onReady);

function onReady() {
    angular.bootstrap(document, ['Validation']);
}
