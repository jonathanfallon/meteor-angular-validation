//"use strict";

Store = new Mongo.Collection('store');

Store.schema = new SimpleSchema({
    first: {
        type: String,
        min: 3,
        max: 12,
        custom: () => {
            return Store.validators.noNumbers(this.value);
        }
    },
    email: {
        type: String,
        unique: true,
        custom: function () {
            if (Meteor.isClient && this.isSet) {
                /**
                 * Validate email format
                 */
                if (!Store.validators.email(this.value)) {
                    Store.simpleSchema().namedContext().addInvalidKeys([{
                        name: "email",
                        type: "regEx"
                    }]);
                }

                /**
                 * Validate email availability
                 */
                Meteor.call("Store.methods.emailIsAvailable", this.value, function (error, result) {
                    if (!result) {
                        Store.simpleSchema().namedContext().addInvalidKeys([{
                            name: "email",
                            type: "notUnique"
                        }]);
                    }
                });
            }
        }
    }
});

/**
 * aldeed:simple-schema attach schema to collection
 */
Store.attachSchema(Store.schema);

/**
 * Publications
 */
if (Meteor.isServer) {
    Meteor.publish("store", function () {
        return Store.find();
    });
}

/**
 * Validation functions
 */
Store.validators = {};

/**
 * Return false when a number is found
 */
Store.validators.noNumbers = (value) => {
    if (/[0-9]+/.test(value)) {
        return false;
    }

    return true;
};

/**
 * Email regEx validation
 * @source http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
 */
Store.validators.email = (value) => {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
};
